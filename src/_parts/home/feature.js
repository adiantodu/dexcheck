import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';

// import AOS from 'aos';
// import "aos/dist/aos.css";

// import Lottie from 'react-lottie';


// import lottie1 from '../../_assets/home/feature-anim-1.json'
// import lottie2 from '../../_assets/home/feature-anim-2.json'
// import lottie3 from '../../_assets/home/feature-anim-3.json'
// import lottie4 from '../../_assets/home/feature-anim-4.json'
// import lottie5 from '../../_assets/home/feature-anim-5.json'
// import lottie6 from '../../_assets/home/feature-anim-6.json'
// import lottie7 from '../../_assets/home/feature-anim-7.json'
// import lottie8 from '../../_assets/home/feature-anim-8.json'

// import vid1 from '../../_assets/home/feature-1.webm'
// import vid2 from '../../_assets/home/feature-2.webm'
// import vid3 from '../../_assets/home/feature-3.webm'
// import vid4 from '../../_assets/home/feature-4.webm'
// import vid5 from '../../_assets/home/feature-5.webm'
// import vid6 from '../../_assets/home/feature-6.webm'
// import vid7 from '../../_assets/home/feature-7.webm'
// import vid8 from '../../_assets/home/feature-8.webm'

// import mini1 from '../../_assets/home/mini-feature1.png'
// import mini2 from '../../_assets/home/mini-feature2.png'
// import mini3 from '../../_assets/home/mini-feature3.png'
// import mini4 from '../../_assets/home/mini-feature4.png'
// import mini5 from '../../_assets/home/mini-feature5.png'
// import mini6 from '../../_assets/home/mini-feature6.png'
// import mini7 from '../../_assets/home/mini-feature7.png'
// import mini8 from '../../_assets/home/mini-feature8.png'

import mini1 from '../../_assets/home/feature1.png'
import mini2 from '../../_assets/home/feature2.png'
import mini3 from '../../_assets/home/feature3.png'
import mini4 from '../../_assets/home/feature4.png'
import mini5 from '../../_assets/home/feature5.png'
import mini6 from '../../_assets/home/feature6.png'
import mini7 from '../../_assets/home/feature7.png'
import mini8 from '../../_assets/home/feature8.png'

export default class Feature extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-center',
        // });
    }

    render() {

    // function defaultOptions(param){
    //     var option = {
    //         loop: true,
    //         autoplay: true,
    //         animationData: param
    //     }
    //     return option
    // }

    return (
        <div className="section section__feature">
            <div className='container'>
                    <div className='section__feature__tittle'>
                        <h1 className='section__feature__tittle__key' data-aos="fade-up">
                            Key Features
                        </h1>
                    </div>
                    

                    <div className='section__feature__content content '>
                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in" >
                            {/* <video poster={mini1} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid1} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie1)} className="integrated__grid__item__icon"/> */}
                            <img src={mini1} alt=""/>
                        </LazyLoad>
                        <div className='content__header-text' data-aos="fade-up">
                            <h1 className='content__header-text__display'>
                                Chain Board
                            </h1>
                            <p className='content__header-text__subdisplay'>
                                An organized data feed that displays the aggregate network data metrics for all the blockchains integrated.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>
                    </div>


                    <div className='section__feature__content content reverse'>

                        <div className='content__header-text' data-aos="fade-up" >
                            <h1 className='content__header-text__display'>
                            NFT Board
                            </h1>
                            <p className='content__header-text__subdisplay'>
                            Navigate through trending NFTs with ease. Providing you with real-time floor and volume data, analytics, all at your fingertips.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>

                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in">
                            {/* <video poster={mini2} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid2} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie2)} className="integrated__grid__item__icon"/> */}
                            <img src={mini2} alt=""/>
                        </LazyLoad>


                    </div>


                    <div className='section__feature__content content '>
                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in">
                            {/* <video poster={mini3} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid3} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie3)} className="integrated__grid__item__icon"/> */}
                            <img src={mini3} alt=""/>
                        </LazyLoad>
                        <div className='content__header-text' data-aos="fade-up"  >
                            <h1 className='content__header-text__display'>
                                DexSwap
                            </h1>
                            <p className='content__header-text__subdisplay'>
                                Make the best trades with the best rates possible across multiple DEXs anytime, anywhere.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>
                    </div>



                <div className='section__feature__content content reverse'>

                    <div className='content__header-text' data-aos="fade-up" >
                        <h1 className='content__header-text__display'>
                        Charts
                        </h1>
                        <p className='content__header-text__subdisplay'>
                        Easily see real-time charts, transactions, and data of your favorite coin.
                        </p>
                        <div className='content__header-text__button-container'>
                            <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                        </div>
                    </div>

                    <LazyLoad height={200} className='content__header-image' data-aos="fade-in" >
                        {/* <video poster={mini4} className="content__header-image__item" autoPlay loop muted>
                            <source src={vid4} type="video/webm"/>
                        </video> */}
                        {/* <Lottie options={defaultOptions(lottie4)} className="integrated__grid__item__icon"/> */}
                        <img src={mini4} alt=""/>
                    </LazyLoad>

                </div>


                    <div className='section__feature__content content '>

                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in" >
                            {/* <video poster={mini5} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid5} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie5)} className="integrated__grid__item__icon"/> */}
                            <img src={mini5} alt=""/>
                        </LazyLoad>

                        <div className='content__header-text' data-aos="fade-up"  >
                            <h1 className='content__header-text__display'>
                            New Tokens
                            </h1>
                            <p className='content__header-text__subdisplay'>
                            Discover new tokens and hidden gems at the same moment they are listed on DEXs.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>



                    </div>



                    <div className='section__feature__content content reverse'>

                        <div className='content__header-text' data-aos="fade-up" >
                            <h1 className='content__header-text__display'>
                            Big Trades
                            </h1>
                            <p className='content__header-text__subdisplay'>
                            Stay up-to-date on the most relevant DEX trades. Be the first to know when the whales buy or sell a coin.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>

                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in" >
                            {/* <video poster={mini6} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid6} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie6)} className="integrated__grid__item__icon"/> */}
                            <img src={mini6} alt=""/>
                        </LazyLoad>

                    </div>


                    <div className='section__feature__content content '>
                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in">
                            {/* <video poster={mini7} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid7} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie7)} className="integrated__grid__item__icon"/> */}
                            <img src={mini7} alt=""/>
                        </LazyLoad>

                        <div className='content__header-text' data-aos="fade-up">
                            <h1 className='content__header-text__display'>
                            Wallet Tracking
                            </h1>
                            <p className='content__header-text__subdisplay'>
                            Manage, track, copy, and save the most profitable wallets all in one place.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>

                    </div>



                    <div className='section__feature__content content reverse'>

                        <div className='content__header-text' data-aos="fade-up" >
                            <h1 className='content__header-text__display'>
                            DexTAX
                            </h1>
                            <p className='content__header-text__subdisplay'>
                            Taking care of your crypto taxes to ensure a hassle-free crypto experience. Whether its an accurate crypto tax report, inventory tracking, or transaction organizing you want to get hold of- DexTax accommodates it all.
                            </p>
                            <div className='content__header-text__button-container'>
                                <button className='content__header-text__button-container__item button--gradientborder'> EXPLORE MORE </button>
                            </div>
                        </div>

                        <LazyLoad height={200} className='content__header-image' data-aos="fade-in">
                            {/* <video poster={mini8} className="content__header-image__item" autoPlay loop muted>
                                <source src={vid8} type="video/webm"/>
                            </video> */}
                            {/* <Lottie options={defaultOptions(lottie8)} className="integrated__grid__item__icon"/> */}
                            <img src={mini8} alt=""/>
                        </LazyLoad>
                    </div>

            </div>
        </div>
        )
    }
}