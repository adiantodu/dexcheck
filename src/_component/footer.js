import React, { Component } from 'react'

import logo from '../_assets/logo-dexcheck.svg'
import Twitter from '../_assets/twitter.svg'
import Tele from '../_assets/telegram.svg'
import Medium from '../_assets/medium.svg'


export class footer extends Component {

    toggleMenu(p){
        p.currentTarget.nextSibling.classList.toggle("active");
        p.currentTarget.classList.toggle("active");
    }

    render() {
    return (
        <>
            <div className='footer section'>
                <div className='container'>
                    <div className='footer__information'>
                        <div className='footer__information__logo'>
                            <a className='top-content-left__logo'>
                                <img alt="" src={logo} className='top-content-left__logo__img'/>
                            </a>
                            <p className='footer__information__logo__paragraft'>In the crypto world, knowledge and information are your shield and sword. Win the battle before stepping in. DexCheck it out! </p>
                            <p className='footer__information__logo__paragraft'>DexCheck | (c) All Rights Reserved | 2022</p>

                        </div>

                        <div className='footer__information__menu'>
                            <div className='footer__information__menu__list'>
                                <h5 className='h5 footer__information__menu__list__title' onClick={(p) => this.toggleMenu(p)}>Analytic</h5>
                                <div className="footer__information__menu__list-wrap">
                                    <a href='#' className='footer__information__menu__list__link'>Chain Board</a>
                                    <a href='#' className='footer__information__menu__list__link'>NFT Board</a>
                                    <a href='#' className='footer__information__menu__list__link'>Charts</a>
                                    <a href='#' className='footer__information__menu__list__link'>New Tokens</a>
                                    <a href='#' className='footer__information__menu__list__link'>Big Trades</a>
                                </div>
                            </div>
                            <div className='footer__information__menu__list'>
                                <h5 className='h5 footer__information__menu__list__title' onClick={(p) => this.toggleMenu(p)}>Tools</h5>
                                <div className="footer__information__menu__list-wrap">
                                    <a href='#' className='footer__information__menu__list__link'>DexSwap</a>
                                    <a href='#' className='footer__information__menu__list__link'>Wallet Tracking</a>
                                    <a href='#' className='footer__information__menu__list__link'>DexTAX</a>
                                </div>
                            </div>
                            <div className='footer__information__menu__list'>
                                <h5 className='h5 footer__information__menu__list__title' onClick={(p) => this.toggleMenu(p)}>Resources</h5>
                                <div className="footer__information__menu__list-wrap">
                                    <a href='#' className='footer__information__menu__list__link'>Pitch Deck</a>
                                    <a href='#' className='footer__information__menu__list__link'>Brand Asset</a>
                                    <a href='#' className='footer__information__menu__list__link'>Audit</a>
                                    <a href='#' className='footer__information__menu__list__link'>Join our Team</a>
                                </div>
                            </div>
                            <div className='footer__information__menu__list'>
                                <h5 className='h5 footer__information__menu__list__title' onClick={(p) => this.toggleMenu(p)}>Social</h5>
                                <div className="footer__information__menu__list-wrap">
                                    <a href='#' className='footer__information__menu__list__link--media'>
                                        <img alt="" src={Twitter} className='footer__information__menu__list__link__social'/>Twitter</a>
                                    <a href='#' className='footer__information__menu__list__link--media'>
                                        <img alt="" src={Tele} className='footer__information__menu__list__link__social'/>Telegram</a>
                                    <a href='#' className='footer__information__menu__list__link--media'>
                                        <img alt="" src={Medium} className='footer__information__menu__list__link__social'/>Medium</a>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>
            </div>
            <div className='footer__copyright'>
                <span>
                    <a href='#' className='footer__copyright__link'>Privacy Policy</a>
                </span>
                <span>
                    <a href='#' className='footer__copyright__link'>Terms of Service</a>
                </span>
                <span>
                    <a href='#' className='footer__copyright__link'>Contact Us</a>
                </span>
            </div>
        </>
        )
    }
}

export default footer 