import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

import Lottie from 'react-lottie';

import visualize_lottie from '../../_assets/home/visualize.json'
import visualize_img from '../../_assets/home/visualize-img.png'

import ico1 from '../../_assets/home/visualize-ico1.png'
import ico2 from '../../_assets/home/visualize-ico2.png'
import ico3 from '../../_assets/home/visualize-ico3.png'
import ico4 from '../../_assets/home/visualize-ico4.png'




export class Visualize extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }


    render() {
        const defaultOptions = {
            loop: true,
            autoplay: true, 
            animationData: visualize_lottie,
            rendererSettings: {
              preserveAspectRatio: 'xMidYMid slice'
            }
          };

        return (
            <div className="section section--grain section__visualize">
                <div className='container'>

                    <label className="section__visualize__label" data-aos="fade-in" data-aos-delay="200">
                        WHAT DOES DEXCHECK DO?
                    </label>
                    <h1 className="section__visualize__heading" data-aos="fade-up">
                        Visualize. Understand. Win.
                    </h1>
                    <p className='section__visualize__detail' data-aos="fade-up">
                        Invest wiser by making data-based decisions. Predict crypto/NFT pricing trends through augmented data tools & analytics.
                    </p>
                    <LazyLoad height={200} className="section__visualize__dexdoimage" data-aos="fade-in" data-aos-delay="100">
                        <Lottie options={defaultOptions} className="section__visualize__dexdoimage__item"/>
                    </LazyLoad>
                    
                    <h1 className="section__visualize__heading--2">
                        Making Analytics Work for You
                    </h1>
                    <div className='section__visualize__card-wrap' data-aos="fade-in" data-aos-delay="100">
                        <LazyLoad height={200} className='section__visualize__card'>
                            <img alt="" src={ico1}/>
                            <label>
                                Simple
                            </label>
                            <p>
                                DexCheck focuses to assist traders by giving comprehensive and up-to-date data on current market patterns.
                            </p>
                        </LazyLoad>
                        <LazyLoad height={200} className='section__visualize__card'>
                            <img alt="" src={ico2}/>
                            <label>
                                Real-time Data
                            </label>
                            <p>
                                DexCheck envisions an easy-to-use unified interface for all DEX needs which will enable users to get real-time data sufficient to update them of newly launched liquidity pools across multiple blockchains thereby maximizing potential earnings.
                            </p>
                        </LazyLoad>
                        <LazyLoad height={200} className='section__visualize__card'>
                            <img alt="" src={ico3}/>
                            <label>
                                Smart Tracking
                            </label>
                            <p>
                                DexCheck aims to help investors and traders in tracking the wallets of their favorite traders/whales as well as obtaining historical and other pertinent data in a visually appealing and well-organized manner.
                            </p>
                        </LazyLoad>
                        <LazyLoad height={200} className='section__visualize__card'>
                            <img alt="" src={ico4}/>
                            <label>
                                DeFi / NFT Edge
                            </label>
                            <p>
                                DexCheck helps you predict crypto/NFT pricing trends through augmented data tools & analytics. Forecast future values and make the safest risks with our cutting-edge data analysis.
                            </p>
                        </LazyLoad>
                    </div>
                </div>
            </div>
        )
    }
}

export default Visualize 