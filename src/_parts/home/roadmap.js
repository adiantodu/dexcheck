import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

import check from '../../_assets/home/roadmap-check.svg'
import calendar from '../../_assets/home/roadmap-calendar.svg'
import dot from '../../_assets/home/roadmap-dot.svg'
import circle from '../../_assets/home/roadmap-circle.svg'

export default class Roadmap extends Component {
    constructor(props) {
        super(props)
        this.state = {
            roadmap:[
                {
                  title: "Q2 2022",
                  data:[
                      {
                        status: 1,
                        value: "Press releases with top crypto news media"
                      },
                      {
                        status: 0,
                        value: "DexSwap aggregator"
                      },
                      {
                        status: 0,
                        value: "Launchpads Presale + DEX/CEX listing"
                      },
                      {
                        status: 0,
                        value: "New Tokens feature"
                      },
                      {
                        status: 0,
                        value: "Ethereum integration"
                      },
                      {
                        status: 0,
                        value: "Big Trades feature"
                      },
                      {
                        status: 0,
                        value: "BNB Chain integration"
                      },
                      {
                        status: 0,
                        value: "Wallet Tracking feature"
                      },
                      {
                        status: 0,
                        value: "Avalanche integration"
                      },
                      {
                        status: 0,
                        value: "Telegram price bot feature"
                      },
                      {
                        status: 0,
                        value: "Fantom integration"
                      },
                      {
                        status: 0,
                        value: "Updates based on community feedbacks"
                      },
                      {
                        status: 0,
                        value: "DexCheck Charts feature"
                      },
                  ]
                },
                {
                    title: "Q3 2022",
                    data:[
                        {
                            status: 0,
                            value: "Limit orders"
                        },
                        {
                            status: 0,
                            value: "HECO integration"
                        },
                        {
                            status: 0,
                            value: "NFT analytics beta"
                        },
                        {
                            status: 0,
                            value: "Aurora integration"
                        },
                        {
                            status: 0,
                            value: "Solana integration"
                        },
                        {
                            status: 0,
                            value: "Arbitrum integration"
                        },
                        {
                            status: 0,
                            value: "Moonriver integration"
                        },
                        {
                            status: 0,
                            value: "Crypto projects promotion tools"
                        },
                        {
                            status: 0,
                            value: "OKX Chain integration"
                        },
                        {
                            status: 0,
                            value: "NFT promotion tools"
                        },
                    ]
                },
                {
                    title: "Q4 2022",
                    data:[
                        {
                            status: 0,
                            value: "DexTAX calculator"
                        },
                        {
                            status: 0,
                            value: "Crypto news feed aggregator feature"
                        },
                        {
                            status: 0,
                            value: "NFT analytics v.1"
                        },
                        {
                            status: 0,
                            value: "New Chains/DEXs integration"
                        },
                        {
                            status: 0,
                            value: "More DEXs integrations"
                        },
                        {
                            status: 0,
                            value: "CEX analytics tools"
                        },
                    ]
                },
                {
                    title: "Q1 2023",
                    data:[
                        {
                            status: 0,
                            value: "Developer API service"
                        },
                        {
                            status: 0,
                            value: "DexCheck automated audit tool"
                        },
                        {
                            status: 0,
                            value: "Smart Money AI tracker"
                        },
                        {
                            status: 0,
                            value: "New Chains/DEXs integration"
                        },
                        {
                            status: 0,
                            value: "CEX analytics tools"
                        },
                    ]
                },
            ]
        }
    }

    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // })
    }
    render() {
        return (
            <div className="section section__roadmap">
                <div className='container'>

                    <h1 className="section__roadmap__heading" data-aos="fade-up">
                        Roadmap
                    </h1>
                    
                    <div>
                        {this.state.roadmap.map((d,k)=>{
                            return(     
                                <LazyLoad height={200} className='roadmap-item' data-aos="fade-in" data-aos-delay="100">
                                    <div className={k === 0 ? 'roadmap-item__float first' : 'roadmap-item__float'}>
                                        {k === 0 ? 
                                            <img src={calendar} alt="" className="roadmap-item__float--active"/>
                                        : (k === 1 ? 
                                            <img src={circle} alt="" className="roadmap-item__float--half"/>
                                            :
                                            <img src={dot} alt="" className="roadmap-item__float--deactive"/>
                                        )}
                                    </div>
                                    <div className='roadmap-item__title'>
                                        {d.title}
                                    </div>
                                    <div className='roadmap-item__box'>
                                        {d.data.map((dd)=>{
                                            return(
                                                <div className='roadmap-item__box__list'>
                                                    <img alt="" src={check} className={dd.status === 1 ? "active" : ''}/>
                                                    <span>
                                                        {dd.value}
                                                    </span>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </LazyLoad>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}