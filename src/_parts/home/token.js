import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

// import coin from '../../_assets/home/coin_animation.webm'
import mini from '../../_assets/home/mini-coin.png'

import ico1 from '../../_assets/home/token-ico1.svg'
import ico2 from '../../_assets/home/token-ico2.svg'
import ico3 from '../../_assets/home/token-ico3.svg'
import ico4 from '../../_assets/home/token-ico4.svg'
import ico5 from '../../_assets/home/token-ico5.svg'



export default class Token extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }
    render() {
        return (
            <div className="section section__token">

                <div className='container'>

                    <h1 className="token__heading" data-aos="fade-up">
                        DexCheck (DXCHECK) Token Utility
                    </h1>
                    <label className="token__label" data-aos="fade-up" data-aos-delay="200">
                        DXCHECK Token is the central driver of the DexCheck platform. 
                    </label>

                    <LazyLoad height={200} className="token__content">
                        {/* <video poster={mini} className="token__content__video" autoPlay loop muted data-aos="fade-in" data-aos-delay="600">
                            <source src={coin} type="video/webm"/>
                        </video> */}
                        <img src={mini} alt="" className="token__content__video"/>
                        <div className="token__content__grid" data-aos="fade-in" data-aos-delay="400">
                            <div className="grid-item">
                                <label className="grid-item__label">
                                    <img alt="" src={ico1}/>
                                    Alpha Unlocks
                                </label>
                                <p className="grid-item__detail">
                                    Hold DXCHECK token to unlock premium platform features/analytics/wallet tracking and much more.
                                </p>
                            </div>
                            <div className="grid-item">
                                <label className="grid-item__label">
                                    <img alt="" src={ico2}/>
                                    Fees Removal
                                </label>
                                <p className="grid-item__detail">
                                    All platform fees (when involved) are removed by holding the DXCHECK token.
                                </p>
                            </div>
                            <div className="grid-item">
                                <label className="grid-item__label">
                                    <img alt="" src={ico3}/>
                                    Exclusive Early Access
                                </label>
                                <p className="grid-item__detail">
                                    DXCHECK token stakers get exclusive early access to new features.
                                </p>
                            </div>
                            <div className="grid-item">
                                <label className="grid-item__label">
                                    <img alt="" src={ico4}/>
                                    DAO Governance Rights
                                </label>
                                <p className="grid-item__detail">
                                    DAO governance rights are given to DXCHECK token holders.
                                </p>
                            </div>
                            <div className="grid-item">
                                <label className="grid-item__label">
                                    <img alt="" src={ico5}/>
                                    Deflationary
                                </label>
                                <p className="grid-item__detail">
                                    Each week DexCheck team buyback + burn the DXCHECK token with half of the fees genereted by the Swap feature making the supply deflationary.
                                </p>
                            </div>
                        </div>
                    </LazyLoad>
                </div>
            </div>
        )
    }
}