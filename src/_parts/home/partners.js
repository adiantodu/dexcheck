import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

import media1 from '../../_assets/home/media1.png'
import media2 from '../../_assets/home/media2.png'
import media3 from '../../_assets/home/media3.png'
import media4 from '../../_assets/home/media4.png'


export default class Partners extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }
    render() {
        return (
            <div className="section section__partner ">
                <div className='container'>

                    <h1 className="section__media__heading" data-aos="fade-up">
                        Partners
                    </h1>

                    <LazyLoad height={200} className="section__media__media-group" data-aos="fade-in" data-aos-delay="100">
                        <img src={media1} alt="" className="section__media__media-group__item"/>
                        <img src={media2} alt="" className="section__media__media-group__item"/>
                        <img src={media3} alt="" className="section__media__media-group__item"/>
                        <img src={media4} alt="" className="section__media__media-group__item"/>
                    </LazyLoad>

                </div>
            </div>
        )
    }
}