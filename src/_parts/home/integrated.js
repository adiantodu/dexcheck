import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

import coin1 from '../../_assets/home/coin-1.svg'
import coin2 from '../../_assets/home/coin-2.svg'
import coin3 from '../../_assets/home/coin-3.svg'
import coin4 from '../../_assets/home/coin-4.svg'
import coin5 from '../../_assets/home/coin-5.svg'
import coin6 from '../../_assets/home/coin-6.svg'
import coin7 from '../../_assets/home/coin-7.svg'
import coin8 from '../../_assets/home/coin-8.svg'
import coin9 from '../../_assets/home/coin-9.svg'
import coin10 from '../../_assets/home/coin-10.svg'
import coin11 from '../../_assets/home/coin-11.svg'

export class Integrated extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }

    render() {
        return (
            <div className="section section--grain section__integrated">
                
                <div className='container'>

                    <h1 className="integrated__heading" data-aos="fade-up">
                        Chains Integrated
                    </h1>
                    <div className="integrated__grid">
                        <LazyLoad height={200} className="integrated__grid__item" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin1} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Ethereum
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin2} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Binance Smart Chain
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin3} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Solana
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin4} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Polygon
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin5} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Avalanche
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin6} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Fantom
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin7} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Moonriver
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin8} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                HECO
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin9} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                OKX Chain
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin10} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Aurora
                            </label>
                        </LazyLoad>
                        <LazyLoad height={200} className="integrated__grid__item soon" data-aos="fade-in" data-aos-delay="200">
                            <img src={coin11} alt="" className="integrated__grid__item__icon"/>
                            <label className="integrated__grid__item__label">
                                Arbitrum
                            </label>
                        </LazyLoad>
                    </div>

                </div>
            </div>
        )
    }
}

export default Integrated 