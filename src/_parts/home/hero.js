import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';

// import Lottie from 'react-lottie';

// import AOS from 'aos';
// import "aos/dist/aos.css";

import mini from '../../_assets/home/mini-hero.png'
import anim from '../../_assets/home/dex_anim.gif'
import vid from '../../_assets/home/dex_anim_min2.webm'
import vid2 from '../../_assets/home/dex_anim_mini.mov'

// import hero_lottie from '../../_assets/home/visualize.json'




export default class Hero extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }

    render() {
      // const defaultOptions = {
      //   loop: true,
      //   autoplay: true, 
      //   animationData: hero_lottie,
      //   rendererSettings: {
      //     preserveAspectRatio: 'xMidYMid slice'
      //   }
      // };
    return (
        <div className="section section__hero">
          <div className='container hero'>

            <div className='hero__header-text'>
                <h1 className='hero__header-text__display' data-aos="fade-up" data-aos-delay="200">
                  Real time DeFi / NFT 
                  <br/>
                  <span className='span--blue'>Analytics</span> and <span className='span--blue'>Insights</span> 
                  <br/>
                  Be <span className='span--white'>ahead</span> of the game
                </h1>
                <p className='hero__header-text__subdisplay' data-aos="fade-up" data-aos-delay="400">
                    Know your way around trading 
                    <br/>
                    <span className='span--whitee'>Cryptocurrencies & NFTs.</span>
                    <br/> 
                    Make decision based on knowledge.
                </p>
            </div>

              <LazyLoad height={200} className='hero__header-image'>
                <video className="hero__header-image__item" poster={mini} autoPlay={true} muted={true} playsInline={true} loop={true}>
                  <source src={vid} type="video/webm"/>
                  <source src={vid2} type="video/mp4; codecs=hvc1"/>
                </video>
                {/* <img src={anim} alt=""/> */}
                {/* <Lottie options={defaultOptions} className="hero__header-image__item"/> */}
              </LazyLoad>

          </div>
      </div>
        )
    }
}