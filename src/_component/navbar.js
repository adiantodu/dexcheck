import React, { Component } from 'react'

import { NavLink } from "react-router-dom";

import logo from '../_assets/logo-dexcheck.svg'
import Twitter from '../_assets/twitter.svg'
import Tele from '../_assets/telegram.svg'
import Medium from '../_assets/medium.svg'
import arrow from '../_assets/arrow-down.svg'
import burger from '../_assets/burger.svg'
import close from '../_assets/close-ico.svg'
import comingsoon from '../_assets/comingsoon.svg'

export default class Nav extends Component {

    componentDidMount(){
        window.onscroll = function() {
            if (window.pageYOffset > 80) {
                document.querySelector('.section__nav').classList.add("active");
            }else{
                document.querySelector('.section__nav').classList.remove("active");
            }
        }
    }

    toggleMenu(p){
        p.currentTarget.classList.toggle("active");
        document.querySelector('.section__nav__menu').classList.toggle("active");
        document.querySelector('.nav--container').classList.toggle("bg");
    }

    toggleDrop(p){
        p.currentTarget.nextSibling.classList.toggle("active");
        p.currentTarget.classList.toggle("active");
    }

    render() {
        return (
            <div className="section section__nav">
                <div className='container nav--container'>
                        <NavLink to='/' className='section__nav__logo' exact>
                            <img alt="" src={logo}/>
                        </NavLink>
                        <div className='section__nav__menu'>
                                <div className='section__nav__menu__link' >
                                    <NavLink to='/' >Features</NavLink>
                                    <NavLink to='/' >Token Utility</NavLink>
                                    <NavLink to='/' >Roadmap</NavLink>
                                    <NavLink to='/' >Partners</NavLink>
                                    <NavLink to='/' >Faq</NavLink>
                                    <div className='section__nav__menu__link__dropdown' onMouseEnter={(p) => this.toggleDrop(p)}>
                                        <div className='section__nav__menu__link__dropdown__head' onClick={(p) => this.toggleDrop(p)}>
                                            Resources
                                            <img alt="" src={arrow} className='section__nav__menu__link__dropdown__head__arrow'/>
                                        </div>
                                        {/* dropdown resource  */}
                                        <div className='section__nav__menu__link__dropdown__body'>
                                            <NavLink to='/' >Pitch Deck</NavLink>
                                            <NavLink to='/' >Audit</NavLink>
                                            <NavLink to='/' >Audit</NavLink>
                                        </div>
                                    </div>
                                </div>

                                <button className='section__nav__menu__btn-launch'>BETA
                                <img src={comingsoon} className='section__nav__menu__btn-launch__coming'/></button>

                                <div className='section__nav__menu__social'>
                                    <a href='#' className='section__nav__menu__social__icon'>
                                        <img alt="" src={Twitter} className=''/></a>
                                    <a href='#' className='section__nav__menu__social__icon'>
                                        <img alt="" src={Tele} className=''/></a>
                                    <a href='#' className='section__nav__menu__social__icon'>
                                        <img alt="" src={Medium} className=''/></a>
                                </div>
                        </div>
                        <button className='section__nav__mobile' onClick={(p) => this.toggleMenu(p)}>
                            <img alt="" src={burger}/>
                            <img alt="" src={close}/>
                        </button>
                </div>
            </div>
        )
    }
}
