// import React, { Component } from 'react'

import Hero from '../_parts/home/hero'
import Media from '../_parts/home/media'
import Visualize from '../_parts/home/visualize'
import Integrated from '../_parts/home/integrated'
import Feature from '../_parts/home/feature'
import Token from '../_parts/home/token'
import Faq from '../_parts/home/faq'
import Footer from '../_component/footer'
import Partners from '../_parts/home/partners'
import Tokenomic from '../_parts/home/tokenomic'
import Roadmap from '../_parts/home/roadmap'
import Nav from '../_component/navbar'

function Home() {
    return (
        <>
            <Nav/>
            <Hero/>
            <Media/>
            <Visualize/>
            <Feature/>
            <Integrated/>
            <Token/>
            <Tokenomic/>
            <Roadmap/>
            <Partners/>
            <Faq/>
            <Footer/>
        </>
    );
}

export default Home;