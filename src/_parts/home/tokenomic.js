import React, { Component } from 'react'
import LazyLoad from 'react-lazyload';
// import AOS from 'aos';
// import "aos/dist/aos.css";

// import img from '../../_assets/home/tokenomic-img.svg'
import img from '../../_assets/home/tokenomic-img.png'

export default class Tokenomic extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-bottom',
        // });
    }
    render() {
        return (
            <div className="section section__tokenomic">
                <div className='container'>


                    <div className="tokenomic__box">

                        <div className="tokenomic__box__top">
                            <div className="tokenomic__box__top__left">
                                <h2 data-aos="fade-up" >
                                    Tokenomics
                                </h2>
                                <div data-aos="fade-up" data-aos-delay="50">
                                    <div className='item'>
                                        Symbol: DXCHECK
                                    </div>
                                    <div className='item'>
                                        Blockchain Network: BSC
                                    </div>
                                    <div className='item'>
                                        Decimals:
                                    </div>
                                    <div className='item'>
                                        Contract:
                                    </div>
                                    <div className='item'>
                                        Token Supply: 100,000,000
                                    </div>
                                    <div className='item'>
                                        Initial Circulating Supply: 20,750,000
                                    </div>
                                </div>
                            </div>
                            <LazyLoad height={200} className='tokenomic__box__top__right' data-aos="fade-in" data-aos-delay="50">
                                <img alt="" src={img}/>
                            </LazyLoad>
                        </div>
                        <div className="tokenomic__box__bot" data-aos="fade-up" data-aos-delay="150">
                            
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#0B61CA"}}></div>
                                <span>
                                    IDO (Public Round): 70.0%
                                </span>
                            </div>
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#7824F3"}}></div>
                                <span>
                                    Open Community Rewards (build2earn): 5.0%
                                </span>
                            </div>
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#E1B8FF"}}></div>
                                <span>
                                    Development & Innovation: 10.0%
                                </span>
                            </div>
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#3FA7E0"}}></div>
                                <span>
                                    Partners/KOLs: 2.5%
                                </span>
                            </div>
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#AF53F4"}}></div>
                                <span>
                                    Marketing: 7.5%
                                </span>
                            </div>
                            <div className='item'>
                                <div className='item__box' style={{backgroundColor:"#018AFF"}}></div>
                                <span>
                                    Liquidity: 5.0%
                                </span>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        )
    }
}