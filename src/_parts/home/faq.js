import React, { Component } from 'react'
// import AOS from 'aos';
// import "aos/dist/aos.css";

import arrow from '../../_assets/arrow-down.svg'



export default class Faq extends Component {
    componentDidMount() {
        // AOS.init({
        //     mirror: true,
        //     once: false,
        //     anchorPlacement: 'top-center',
        // });
    }

    toggleAccord(p){
        p.currentTarget.classList.toggle("active");
    }

    render() {
        return (
            <div className="section section__faq">
            <div className='container'>
                <h1 className='section__faq__tittle' data-aos="fade-up">
                    Frequently Asked Questions
                </h1>
                <div className='section__faq__item'>

                        {/* faq item acordion 1 */}
                        <div className='accordion' onClick={(p) => this.toggleAccord(p)} data-aos="fade-in">
                            <div className='accordion__head'>
                                <h1 className='accordion__head__tittle'>
                                    What is DexCheck ?
                                </h1>
                                <div className='accordion__head__icon'>
                                    <img alt="" src={arrow}/>
                                </div>
                            </div>
                            <div className='accordion__body'>
                                <p>
                                DexCheck is a web platform providing numerous analytical tools for cryptocurrencies trading as well as NFTs.
                                </p>
                            </div>
                        </div>

                        {/* faq item acordion 2 */}
                        <div className='accordion' onClick={(p) => this.toggleAccord(p)} data-aos="fade-in">
                            <div className='accordion__head'>
                                <h1 className='accordion__head__tittle'>
                                What is DexCheck used for ? 
                                </h1>
                                <div className='accordion__head__icon'>
                                    <img alt="" src={arrow}/>
                                </div>
                            </div>
                            <div className='accordion__body'>
                                <p>
                                The analytical tools can be used by both retail and institutional investors to alleviate their trading risk and maximize the profits.
                                </p>
                            </div>
                        </div>

                        {/* faq item acordion 3 */}
                        <div className='accordion' onClick={(p) => this.toggleAccord(p)} data-aos="fade-in">
                            <div className='accordion__head'>
                                <h1 className='accordion__head__tittle'>
                                How much does it cost to use DexCheck ?
                                </h1>
                                <div className='accordion__head__icon'>
                                    <img alt="" src={arrow}/>
                                </div>
                            </div>
                            <div className='accordion__body'>
                                <p>
                                Most features on DexCheck are free of cost and do not require any sort of payment. However, some unique features require certain holding of DXCHECK tokens. 
                                </p>
                            </div>
                        </div>

                        {/* faq item acordion 4 */}
                        <div className='accordion' onClick={(p) => this.toggleAccord(p)} data-aos="fade-in">
                            <div className='accordion__head'>
                                <h1 className='accordion__head__tittle'>
                                Does DexCheck have a token ?
                                </h1>
                                <div className='accordion__head__icon'>
                                    <img alt="" src={arrow}/>
                                </div>
                            </div>
                            <div className='accordion__body'>
                                <p>
                                Yes, DexCheck have a token named DXCheck. Users can buy DXCHECK token directly from DexSwap aggregator feature or through PancakeSwap.
                                </p>
                            </div>
                        </div>

                        {/* faq item acordion 5 */}
                        <div className='accordion' onClick={(p) => this.toggleAccord(p)} data-aos="fade-in">
                            <div className='accordion__head'>
                                <h1 className='accordion__head__tittle'>
                                Do I need to connect my wallet to use DexCheck?
                                </h1>
                                <div className='accordion__head__icon'>
                                    <img alt="" src={arrow}/>
                                </div>
                            </div>
                            <div className='accordion__body'>
                                <p>
                                No, you don’t need to connect your wallet to use DexCheck; however some features will be limited. We encourage users to connect their wallets with DXCHECK tokens to gain full access to all the DexCheck tools.
                                </p>
                            </div>
                        </div>

                </div>
            </div>
        </div>
        )
    }
}
